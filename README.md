## docker-android-nodejs

Based on `slickstars/docker-android:1.1`

### ENV VARIABLES
- **NODE_VERSION** (6) - NodeJS version to install

## docker-android
The image installs android sdk `30.0.2` and minimal plugins and addons.

### ENV VARIABLES
- **ANDROID_BUILD_TOOLS_VERSION** (30.0.2) - Android SDK version (build tools)

Note: Build tools version currently is fixed only.

## docker-java
Uses `openjdk-8-jdk-headless`

## docker-base
Based on ubuntu:18.04 LTS

